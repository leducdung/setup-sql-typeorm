import { IsBoolean, IsDate } from 'class-validator'
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm'

@Entity({ name: 'verification' })
export class Verification extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  verificationId: string

  @Column({ name: 'userId', type: 'uuid', nullable: false })
  userId: string

  @Column({ name: 'expiryDate', type: Date, nullable: false })
  @IsDate()
  expiryDate: Date

  @Column({ name: 'isUsed', type: 'boolean', nullable: false, default: false })
  @IsBoolean()
  isUsed!: boolean

  @Column({ name: 'createdAt', type: 'timestamp', nullable: true, default: () => 'CURRENT_TIMESTAMP' })
  @IsDate()
  createdAt: Date

  @Column({ name: 'updatedAt', type: 'timestamp', nullable: true, default: null })
  @IsDate()
  updatedAt: Date
}
