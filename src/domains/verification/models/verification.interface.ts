export interface IVerification {
  verificationId: string
  userId: string
  expiryDate: Date
  isUsed: boolean
}

export interface ICreateUserVerificationService {
  userId: string
}