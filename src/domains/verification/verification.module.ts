import { Module } from '@nestjs/common'
import { VerificationService } from './verification.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from '../../configs/configs.module'
import { Verification } from './models/verification.schema'
import { DatabaseModule } from '../../database/database.module'

const VerificationRepository = TypeOrmModule.forFeature([Verification])

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
    VerificationRepository,
  ],
  controllers: [],
  providers: [VerificationService],
  exports: [
    VerificationService,
    VerificationRepository,
  ],
})

export class UserVerificationModule { }