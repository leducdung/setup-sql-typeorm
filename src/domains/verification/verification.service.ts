import { EntityRepository, Raw, Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { Verification } from './models/verification.schema'
import { IVerification, ICreateUserVerificationService } from './models/verification.interface'

@EntityRepository(Verification)
export class VerificationService {
  constructor(
    @InjectRepository(Verification)
    private readonly verificationRepository: Repository<Verification>,
  ) { }

  async createUserVerification(userInfo: ICreateUserVerificationService): Promise<IVerification> {
    try {
      let userVerification = await this.verificationRepository.findOne({
        where: {
          userId: userInfo.userId,
          expiryDate: Raw(alias => `${alias} > NOW()`),
          isUsed: false
        }
      })
      if (!userVerification) {
        userVerification = await this.verificationRepository.save({
          userId: userInfo.userId,
          expiryDate: new Date(Date.now() + 30 * 60000)
        })
      }

      return userVerification
    } catch (error) {
      return Promise.reject(error)
    }
  }
}