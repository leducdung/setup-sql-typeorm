import { EntityRepository, Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from './models/users.schema'
import { ICreateUserService, IUser } from './models/users.interface'
import * as bcrypt from 'bcryptjs'
import { ConfigService } from '../../configs/configs.service'
import { VerificationService } from '../verification/verification.service'

@EntityRepository(User)
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly configService: ConfigService,
    private readonly verificationService: VerificationService
  ) { }

  async createOne({ createData }: ICreateUserService): Promise<IUser> {
    try {
      const isExitsUser = await this.userRepository.findOne({
        where: { email: createData.email }
      })
      if (isExitsUser) {
        return Promise.reject({
          name: 'EmailExists',
          code: 400
        })
      }

      const hashPass = await bcrypt.hash(createData.password, this.configService.bcryptSalt)

      createData = {
        ...createData,
        password: hashPass
      }

      const newUser = await this.userRepository.save(createData)

      await this.verificationService.createUserVerification({ userId: newUser.userId })

      return newUser
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
