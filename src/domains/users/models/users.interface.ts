import { CountryCode } from '../enums/types'
import { UserGender } from '../enums/user-gender'

export interface IUser {
  id: string
  email: string
  firstName: string
  lastName: string
  gender: UserGender
  countryCode: CountryCode
  mobilePrefix: string
  mobile: string
  dateOfBirth: Date
  profilePicUrl: string
}

export interface ICreateUserService {
  createData
}
