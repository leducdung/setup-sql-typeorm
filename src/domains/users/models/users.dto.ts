import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  IsString,
  IsEmail,
  IsOptional,
  Length,
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsEnum,
  IsDate
} from 'class-validator'
import { Exclude, Transform, TransformFnParams, Type } from 'class-transformer'
import { UserGender } from '../enums/user-gender'
import { CountryCode } from '../enums/types'

export class RegisterUserDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly firstName?: string = ''

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly lastName?: string = ''

  @ApiProperty()
  @IsEmail()
  @Transform(({ key, obj }: TransformFnParams) => `${obj[key]}`.toLowerCase())
  @IsString()
  email: string

  @ApiProperty()
  @IsString()
  password: string
}

export class UserSingupRequestBody {
  @ApiProperty({
    description: 'Unique email address',
    required: true,
    example: 'sample@sample.com'
  })
  @IsNotEmpty()
  @Length(1, 320)
  @IsEmail()
  @Transform(({ key, obj }: TransformFnParams) => `${obj[key]}`.toLowerCase())
  email: string

  @ApiProperty({
    description: 'password',
    required: true,
    example: 'yourSecretPassword'
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(8, {
    message: 'Password is too short. Need at least 8 charter.'
  })
  @MaxLength(20, {
    message: 'Password is too Large. Not more then 20 charter.'
  })
  @Exclude({ toPlainOnly: true })
  password: string

  @ApiProperty({
    description: 'User first name',
    required: true,
    example: 'First name'
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 50)
  firstName: string

  @ApiProperty({
    description: 'User last name',
    required: true,
    example: 'Last name'
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 50)
  lastName: string

  @ApiPropertyOptional({
    description: 'Gender',
    enum: UserGender
  })
  @IsOptional()
  @IsEnum(UserGender)
  gender?: UserGender

  @ApiProperty({
    description: 'Country Code',
    required: true,
    enum: CountryCode
  })
  @IsNotEmpty()
  @Length(1, 10)
  @IsEnum(CountryCode)
  countryCode: CountryCode

  @ApiProperty({
    description: 'mobile prefix',
    required: true,
    example: '+1'
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 10)
  mobilePrefix: string

  @ApiProperty({
    description: 'mobile prefix',
    required: true,
    example: '012-3458-3721'
  })
  @IsNotEmpty()
  @IsString()
  @Length(1, 20)
  mobile: string

  @ApiProperty({
    description: 'User birth date',
    required: true,
    example: '1990-01-01'
  })
  @Type(() => Date)
  @IsDate()
  dateOfBirth: Date

  @ApiPropertyOptional({
    description: 'User Profile pic url',
    example: '/user/profilePic'
  })
  @IsOptional()
  @Length(0, 255)
  @IsString()
  profilePicUrl?: string
}
