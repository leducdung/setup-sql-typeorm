import { IsBoolean, IsDate, IsEmail, IsEnum, IsOptional, Length } from 'class-validator'
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm'
import { CountryCode } from '../enums/types'
import { UserGender } from '../enums/user-gender'

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  userId: string

  @Column({ name: 'email', type: 'varchar', length: 320, nullable: false })
  @Length(0, 320)
  @IsEmail()
  email: string

  @Column({ name: 'password', type: 'varchar', length: 100, nullable: false, select: false })
  @Length(0, 100)
  password: string

  @Column({ name: 'firstName', type: 'varchar', length: 50, nullable: false })
  @Length(0, 50)
  firstName: string

  @Column({ name: 'lastName', type: 'varchar', length: 50, nullable: false })
  @Length(0, 50)
  lastName: string

  @Column({ name: 'gender', type: 'varchar', length: 10, nullable: true })
  @IsEnum(UserGender)
  @IsOptional()
  gender: UserGender

  @Column({ name: 'countryCode', type: 'varchar', length: 10, nullable: false })
  @IsEnum(CountryCode)
  countryCode: CountryCode

  @Column({ name: 'mobilePrefix', type: 'varchar', length: 10, nullable: false })
  @Length(0, 10)
  mobilePrefix: string

  @Column({ name: 'mobile', type: 'varchar', length: 20, nullable: false })
  @Length(0, 20)
  mobile: string

  @Column({ name: 'dateOfBirth', type: 'date', nullable: false })
  @IsDate()
  dateOfBirth: Date

  @Column({ name: 'profilePicUrl', type: 'varchar', length: 255, nullable: true, default: null })
  @IsOptional()
  @Length(0, 255)
  profilePicUrl: string

  @Column({
    name: 'isEmailConfirm',
    type: 'boolean',
    nullable: false,
    default: false
  })
  @IsBoolean()
  isEmailConfirm: boolean

  @Column({
    name: 'isDelete',
    type: 'boolean',
    nullable: false,
    default: false
  })
  @IsBoolean()
  isDelete: boolean

  @Column({ type: 'varchar', nullable: true })
  test?: string

  @Column({ type: 'varchar', nullable: true })
  test2?: string

  @Column({ type: 'varchar', nullable: true })
  test3?: string

  @Column({ name: 'createdAt', type: 'timestamp', nullable: true, default: () => 'CURRENT_TIMESTAMP' })
  @IsDate()
  createdAt: Date

  @Column({ name: 'updatedAt', type: 'timestamp', nullable: true, default: null })
  @IsDate()
  updatedAt: Date
}
