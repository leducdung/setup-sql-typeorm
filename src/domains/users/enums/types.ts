export enum UserType {
  CUSTOMER = 'CUSTOMER',
  ADMIN = 'ADMIN'
}

export enum CountryCode {
  Vietnam = 'VNM',
  Korea = 'KOR',
  America = 'USA'
}
