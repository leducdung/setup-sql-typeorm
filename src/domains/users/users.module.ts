import { Module } from '@nestjs/common'
import { UserController } from './users.controller'
import { UsersService } from './users.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule } from '../../configs/configs.module'
import { User } from './models/users.schema'
import { DatabaseModule } from '../../database/database.module'
import { UserVerificationModule } from '../verification/verification.module'

const UserRepository = TypeOrmModule.forFeature([User])

@Module({
  imports: [
    UserVerificationModule,
    ConfigModule,
    DatabaseModule,
    UserRepository
  ],
  controllers: [UserController],
  providers: [UsersService],
  exports: [
    UsersService,
    UserRepository,
  ],
})

export class UserModule { }
