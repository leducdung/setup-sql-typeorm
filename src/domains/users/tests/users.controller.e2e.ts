import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from '../../../app.module'

describe('UserController (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
  })

  // just example
  it('/ (GET)', async () => {
    const { statusCode } = await request(app.getHttpServer())
      .post('/auth/signup')
      .send({
        firstName: 'a',
        lastName: 'b',
        email: 'test@gmail.com',
        password: 'Aa@123456'
      })

    expect(statusCode).toEqual(200)
  })
})
