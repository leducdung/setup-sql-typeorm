import { Controller, Post, Body } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { checkControllerErrors } from '../../shared/checkErrors'
import { UserSingupRequestBody } from './models/users.dto'
import { IUser } from './models/users.interface'
import { UsersService } from './users.service'

@ApiTags('Users')
@Controller()
export class UserController {
  constructor(private readonly userService: UsersService) { }
  @Post('/auth/signup')
  @ApiOperation({ description: 'To register a new user account' })
  async registerUser(@Body() userSingupRequestBody: UserSingupRequestBody): Promise<IUser> {
    try {
      const newUser = await this.userService.createOne({
        createData: userSingupRequestBody
      })

      return newUser
    } catch (error) {
      checkControllerErrors(error)
    }
  }
}
