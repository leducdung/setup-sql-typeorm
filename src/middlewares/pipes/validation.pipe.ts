import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'
import { ConfigService } from '../../configs/configs.service'
import { PRODUCTION } from '../../common/constants'

const configService = new ConfigService()

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value
    }

    const object = plainToClass(metatype, value)

    const errors = await validate(object, {
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    })

    if (configService.nodeEnv !== PRODUCTION && errors.length > 0) {
      throw new BadRequestException(errors)
    }

    if (errors.length > 0) {
      throw new BadRequestException('Validation failed')
    }

    return object
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object]

    return !types.includes(metatype)
  }
}
