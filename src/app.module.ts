import { Module } from '@nestjs/common'
import { DatabaseModule } from './database/database.module'
import { UserModule } from './domains/users/users.module'
import { UserVerificationModule } from './domains/verification/verification.module'

@Module({
  imports: [
    DatabaseModule,
    UserModule,
    UserVerificationModule
  ]
})
export class AppModule { }
