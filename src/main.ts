import { NestFactory } from '@nestjs/core'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { ConfigService } from './configs/configs.service'
import { AppModule } from './app.module'
import { ValidationPipe } from './middlewares/pipes/validation.pipe'

const configService = new ConfigService()

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  app.enableCors()

  const config = new DocumentBuilder()
    .setTitle('TeraComix App')
    .setDescription('The TeraComix APIs description')
    .setVersion('1.0')
    .addTag('TeraComix APIs')
    .build()

  const document = SwaggerModule.createDocument(app, config)

  SwaggerModule.setup('api', app, document)

  app.useGlobalPipes(new ValidationPipe())

  await app.listen(
    configService.internalPort,
    configService.internalHost,
    // log cb
    () => console.log(`Our app is started at ${configService.internalHost}:${configService.internalPort}`),
  )
}

bootstrap()
