import { InternalServerErrorException, BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common'
import { PRODUCTION } from '../common/constants'
import { notFoundErrors, badRequestErrors, forbiddenErrors } from '../common/errors'
import { ConfigService } from '../configs/configs.service'

const configService = new ConfigService()
const checkControllerErrorsInLocal = (error: any): void => {
  if (error.code === 'ER_DUP_ENTRY' && error.name === 'QueryFailedError') {
    console.log(`${error.message} - ${error.stack}`)
    throw new BadRequestException('Duplicated email')
  }

  if (error.code === 403 || error.status === 403) {
    console.log(`${error.message} - ${error.stack}`)
    throw new ForbiddenException(forbiddenErrors[error.name])
  }

  if (error.code === 404 || error.status === 404) {
    console.log(`${error.message} - ${error.stack}`)
    throw new NotFoundException(error)
  }

  if (error.code === 400 || error.status === 400) {
    console.log(`${error.message} - ${error.stack}`)
    throw new BadRequestException(error)
  }

  console.log(`${error.message} - ${error.stack}`)

  throw new InternalServerErrorException({
    error: `${error.message} - ${error.stack}`,
  })
}

const checkControllerErrorsInProd = (error: any): void => {
  if (error.code === 'ER_DUP_ENTRY' && error.name === 'QueryFailedError') {
    throw new BadRequestException('Duplicated email')
  }

  if (error.name === 'ValidationError' && error.code === 403) {
    throw new ForbiddenException('No Permissions')
  }

  if (error.code === 404) {
    throw new NotFoundException(notFoundErrors[error.name])
  }

  if (error.code === 400) {
    throw new BadRequestException(badRequestErrors[error.name])
  }

  throw new InternalServerErrorException('Something wrong happens')
}

export const checkControllerErrors = configService.nodeEnv !== PRODUCTION
    ? checkControllerErrorsInLocal
    : checkControllerErrorsInProd
