import * as dotenv from 'dotenv'
import * as Joi from 'joi'
import * as fs from 'fs'

import { DEVELOPMENT, TEST, PRODUCTION, LOCAL } from '../common/constants'

export interface EnvConfig {
  [key: string]: string
}

// Note: default env is development.
export class ConfigService {
  private readonly envConfig: EnvConfig

  constructor() {
    const nodeEnv = process?.env?.NODE_ENV || LOCAL

    const configFilePath = `environments/.${nodeEnv.toLocaleLowerCase()}.env`

    const hasEnvFile = fs.existsSync(configFilePath)

    if (!hasEnvFile) {
      throw Error(`environment <${nodeEnv}> file not exists!`)
    }

    const config = dotenv.parse(fs.readFileSync(configFilePath))

    this.envConfig = this.validateInput({
      ...config,
      NODE_ENV: nodeEnv,
    })
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      // node env
      NODE_ENV: Joi.string().valid(DEVELOPMENT, PRODUCTION, TEST, LOCAL).default(LOCAL),

      // host && port
      INTERNAL_HOST: Joi.string().required(),
      INTERNAL_PORT: Joi.number().required(),

      // typeorm
      TYPEORM_CONNECTION: Joi.string().required(),
      TYPEORM_HOST: Joi.string().required(),
      TYPEORM_USERNAME: Joi.string().required(),
      TYPEORM_PASSWORD: Joi.string().required(),
      TYPEORM_DATABASE: Joi.string().required(),
      TYPEORM_PORT: Joi.number().required(),
      TYPEORM_SYNCHRONIZE: Joi.string().valid('true', 'false'),
      TYPEORM_SCHEMA: Joi.string().valid('true', 'false'),
      TYPEORM_MIGRATIONS: Joi.string().required(),
      TYPEORM_MIGRATIONS_RUN: Joi.string().valid('true', 'false'),
      TYPEORM_MIGRATIONS_DIR: Joi.string().required(),
      TYPEORM_ENTITIES: Joi.string().required(),
      // jwt
      JWT_COMFIRM_SECRET: Joi.string().required(),
      JWT_SECRET: Joi.string().required(),

      // bcrypt
      BCRYPT_SALT: Joi.number().required()
    })

    const { error, value: validatedEnvConfig } = envVarsSchema.validate(envConfig)

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    const jwtComfirmSecret = String(validatedEnvConfig.JWT_COMFIRM_SECRET)
    const jwtSecret = String(validatedEnvConfig.JWT_SECRET)

    if (jwtComfirmSecret === jwtSecret) {
      throw new Error('Jwt secret key and jwt comfirm secret key must be different')
    }

    return validatedEnvConfig
  }

  get nodeEnv(): string {
    return String(this.envConfig.NODE_ENV)
  }

  get internalHost(): string {
    return String(this.envConfig.INTERNAL_HOST)
  }

  get internalPort(): number {
    return Number(this.envConfig.INTERNAL_PORT)
  }

  get getJwtComfirmSecret(): string {
    return String(this.envConfig.JWT_COMFIRM_SECRET)
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET)
  }

  get bcryptSalt(): number {
    return Number(this.envConfig.BCRYPT_SALT)
  }

  get databaseConfig() {
    const dbConfig = {
      type: String(this.envConfig.TYPEORM_CONNECTION),
      host: String(this.envConfig.TYPEORM_HOST),
      port: Number(this.envConfig.TYPEORM_PORT),
      username: String(this.envConfig.TYPEORM_USERNAME),
      password: String(this.envConfig.TYPEORM_PASSWORD),
      database: String(this.envConfig.TYPEORM_DATABASE),
      entities: [String(this.envConfig.TYPEORM_ENTITIES)],
      autoSchemaSync: String(this.envConfig.TYPEORM_SCHEMA) === 'true',
      synchronize: String(this.envConfig.TYPEORM_SYNCHRONIZE) === 'true',
      migrations: [String(this.envConfig.TYPEORM_MIGRATIONS)],
      migrationsRun: String(this.envConfig.TYPEORM_MIGRATIONS_RUN) === 'true',
      logging: String(this.envConfig.TYPEORM_LOGGING) === 'true',
      cli: {
        migrationsDir: String(this.envConfig.TYPEORM_MIGRATIONS_DIR),
      },
    }

    return Object(dbConfig)
  }
}
