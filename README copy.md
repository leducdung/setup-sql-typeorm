# teraComix-backend-client

## Project structures

```
.
├── environments/ * Environments folder
|   └── .env.sample
├── migrations/  * Migrations folder
|   └── helpers/
|   └── scripts/
├── ops/ * Ops folder
├── src/
|   ├── common/ * Common folder
|   |   └── constants.ts
|   |   └── errors.ts
|   ├── configs/ * The config module
|   ├── database/ * The database module / Database configuration
|   ├── domains/
|   |   |── module_name
|   |      |── models
|   |      |    └── module_name.dto.ts  * Dto for module/endpoint
|   |      |    └── module_name.interface.ts  * Interface for module/endpoint
|   |      |    └── module_name.schema.ts  * Entity/Schema for module/endpoint
|   |      |── tests                
|   |      |    └── module_name.controller.e2e.ts    * E2e test file for module/endpoint
|   |      └── module_name.controller.ts  * Controller for module/endpoint
|   |      └── module_name.controller.ts  * Route/Controller for module/endpoint
|   |      └── module_name.module.ts      * Module file for module/endpoint
|   |      └── module_name.service.ts     * Service file for module/endpoint
|   ├── helpers/ * Helper folder
|   |   |── get.ts
|   |   |── build.ts
|   ├── middlewares/ * Middlewares folder (Auth/Authz)
|   |   |── auth
|   |   |  |── models
|   |   |  |    └── auth.interface.ts
|   |   |  |── strategies * Strategies folder
|   |   |  |── auth.module.ts
|   |   |  └── auth.service.ts * The service use to decode user's token 
|   |   |── authz
|   |   |  |── authz.service.ts * The service use to validate user's scopes 
|   |   |── pipes
|   |   |  |── validation.pipe.ts 
|   ├── shared/ * shared folder
|   ├── app.module.ts
|   ├── main.ts/ * Server run file
└── package.json
└── package-lock.json
```

## Installation

```bash
npm install
```

## How to run server

1. npm install
2. npm run build
3. npm run start:dev
4. Server will run on `http://localhost:3000` and API's doc is http://localhost:3000/api

## Migrations

- Migration dir: ./migrations

- Create migration file:

```bash
  - Dev environment
  $ npm run migrate:create:dev <migration filename>
```

- Run migrations:

```bash
  - Dev environment
  $ npm run migrate:up:dev
```

- Revert migrations:

```bash
  - Dev environment
  $ npm run migrate:revert:dev
```

## Tests

- Run tests:

```bash
  - Test dev environment
  $ npm run test:e2e
```

## Debug

* configurations in Vscode, `launch.json` file

```bash

{
  "version": "1.0.0",
  "configurations": [
    {
      "name": "NestJS teraComix debug",
      "type": "node",
      "request": "launch",
      "args": ["${workspaceFolder}/src/main.ts"],
      "runtimeArgs": [
        "--nolazy",
        "-r",
        "ts-node/register",
        "-r",
        "tsconfig-paths/register"
      ],
      "sourceMaps": true,
      "cwd": "${workspaceRoot}",
      "protocol": "inspector",
      # "env": {"NODE_ENV":"development"}, >>>> note: update the env before debugging
      "console": "integratedTerminal"
    }
  ]
}
```

## Swagger
> http://localhost:3000/api

### References
* Docs - [https://docs.nestjs.com](https://docs.nestjs.com/)
