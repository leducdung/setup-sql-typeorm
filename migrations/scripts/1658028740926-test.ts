import {MigrationInterface, QueryRunner} from "typeorm";

export class test1658028740926 implements MigrationInterface {
    name = 'test1658028740926'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`mevi\`.\`user\` (\`userId\` char(36) NOT NULL, \`email\` varchar(320) NOT NULL, \`password\` varchar(100) NOT NULL, \`firstName\` varchar(50) NOT NULL, \`lastName\` varchar(50) NOT NULL, \`gender\` varchar(10) NULL, \`countryCode\` varchar(10) NOT NULL, \`mobilePrefix\` varchar(10) NOT NULL, \`mobile\` varchar(20) NOT NULL, \`dateOfBirth\` date NOT NULL, \`profilePicUrl\` varchar(255) NULL, \`isEmailConfirm\` tinyint NOT NULL DEFAULT 0, \`isDelete\` tinyint NOT NULL DEFAULT 0, \`test\` varchar(255) NULL, \`test2\` varchar(255) NULL, \`test3\` varchar(255) NULL, \`createdAt\` timestamp NULL DEFAULT CURRENT_TIMESTAMP, \`updatedAt\` timestamp NULL, PRIMARY KEY (\`userId\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`mevi\`.\`verification\` (\`verificationId\` char(36) NOT NULL, \`userId\` char(36) NOT NULL, \`expiryDate\` datetime NOT NULL, \`isUsed\` tinyint NOT NULL DEFAULT 0, \`createdAt\` timestamp NULL DEFAULT CURRENT_TIMESTAMP, \`updatedAt\` timestamp NULL, PRIMARY KEY (\`verificationId\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`mevi\`.\`verification\``);
        await queryRunner.query(`DROP TABLE \`mevi\`.\`user\``);
    }

}
