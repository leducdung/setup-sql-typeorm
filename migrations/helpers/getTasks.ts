export const getTasks = (tasks) => {
    return () => {
      const task = tasks.shift()

      if (!task) {
        return
      }

      return task
    }
}
