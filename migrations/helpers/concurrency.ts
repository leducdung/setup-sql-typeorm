import EventEmitter from 'events'

/**
 * Used for executing tasks concurrently
 *
 * enqueue function
 * - Return a promise which will be executed
 *
 * @param {integer}   numConcurrentTasks   Number of concurrent tasks
 * @param {function}  getTask              Function will be called to enqueue task, will return any available task
 *
 * @return {object}
 */

export const run = async (numConcurrentTasks, getTask, totalTasks) => {
    // TO DO: Implement this function to process tasks concurrently
    // For example, if there are 10 tasks in total and there should be 3 concurrently tasks:
    // - At the first step: the task 1, 2, 3 must start immediately.
    // - If any task is done, the next task musts start immediately.
    // - After the last task starts, there must be no more queueing up.
    const processingEmitter = new EventEmitter()

    let numberOfDoneTasks = 0

    const waitForAllDone = () => new Promise( (resolve, reject) => {
      console.log('allDone before emitting')
      if (numberOfDoneTasks === totalTasks) {
        console.log('allDone')
        resolve('done')
      }
      processingEmitter.once('allDone', e =>  {
        console.log('allDone')
        resolve('done') // done
      })
    })

    processingEmitter.on('oneTaskDone', () => {
      numberOfDoneTasks++
      console.log('numberOfDoneTasks', numberOfDoneTasks)
      const task = getTask()

      if (!task && numberOfDoneTasks < totalTasks) {
        return
      }

      if (numberOfDoneTasks === totalTasks) {
        console.log('numberOfDoneTasks === totalTasks')
        return processingEmitter.emit('allDone')
      }

      task()
        .then(() => processingEmitter.emit('oneTaskDone'))
    })

    try {
      const initalTasks = []
      for (let i = 1; i <= numConcurrentTasks; i++) {
        initalTasks.push(
          getTask()()
            .then(() => processingEmitter.emit('oneTaskDone')),
        )
      }

      await Promise.all(initalTasks)

      await waitForAllDone()
    } catch (error) {
      Promise.reject(error)
    }
  }
